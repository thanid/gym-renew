<div class="bg-section-5">
    <div class="container section-1 footer">
        <div class="name-band text-center">
            <div class="logo justify-content-center">
            <?php
                echo file_get_contents("assets/images/logo.svg");
            ?>
            </div>
            <p class="text-under-name-head text-footer">Design by Wolves Corporation</p>
        </div>
    </div> 
</div>