<div class="bg-section-1">
    <div class="container section-1">
        <div class="name-band text-center">
            <div class="logo justify-content-center">
                <?php
                    echo file_get_contents("assets/images/logo.svg");
                ?>
                <!-- <img src="assets/images/logo.svg" alt="" class="w-100"> -->
            </div>
            <p class="text-under-name-section-1" data-aos="fade-up">Website e-commerce</p>
        </div>
        <div class="row group-under-logo mt-5">
            <div class="col-12 col-md-6 col-lg-6 w3-animate-left">
                <div class="group-text-section-1-left">
                    <p class="text-section-1">Nice work...It's awesome! ...</p>
                    <p class="text-section-1 text-section-1-left">I really like your work, it's great.</p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6 w3-animate-right">
                <div class="group-text-section-1-right">
                    <p class="text-section-1">Please check out my work. FOLLOW</p>
                    <p class="text-section-1 text-section-1-right">ME. I hope you will like</p>
                    <p class="text-section-1 text-section-1-right">my work appreciate me and Get</p>
                </div>
            </div>
        </div>


        <div class="image-page-section-1 w3-animate-bottom">
            <img src="assets/images/section-1/page-1.png" alt="" class="">
        </div>
    </div> 
</div>